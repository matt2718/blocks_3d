#pragma once

#include "Bigfloat.hxx"

#include <Eigen/LU>

using Eigen_Matrix = Eigen::Matrix<Bigfloat, Eigen::Dynamic, Eigen::Dynamic>;
using Eigen_Vector = Eigen::Matrix<Bigfloat, Eigen::Dynamic, 1>;
using Eigen_Diagonal = Eigen::DiagonalMatrix<Bigfloat, Eigen::Dynamic>;
