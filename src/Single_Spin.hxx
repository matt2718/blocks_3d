#pragma once

#include "Bigfloat.hxx"
#include "ostream_vector.hxx"

#include <boost/math/tools/polynomial.hpp>

struct Single_Spin
{
  std::vector<boost::math::tools::polynomial<Bigfloat>> numerator;
};

inline std::ostream &operator<<(std::ostream &os, const Single_Spin &spin)
{
  return os << spin.numerator;
}
