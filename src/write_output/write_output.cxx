#include "Transforms.hxx"
#include "../Single_Spin.hxx"
#include "../Q4pm.hxx"
#include "../Coordinate.hxx"

#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
#include <fmt/format.h>

#include <set>
#include <map>
#include <thread>
#include <atomic>

void write_header(
  boost::filesystem::ofstream &outfile, const std::string &output_template,
  const std::array<int64_t, 4> &two_js, const int64_t &two_j_intermediate,
  const std::set<int64_t> &two_js_intermediate, const int64_t &two_j12,
  const int64_t &two_j43, const std::string &delta_12_string,
  const std::string &delta_43_string, const std::string &delta_1_plus_2_string,
  const Q4pm &q4pm, const int64_t &order, const int64_t &lambda,
  const std::set<Coordinate> &coordinates, const int64_t &kept_pole_order,
  const std::set<Bigfloat> &output_poles, const size_t &precision_base_2,
  const int &argc, char *argv[]);

std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
rho_rhob_to_zzb(
  const int64_t &parity, const size_t &two_j_index, const size_t &parity_index,
  const size_t &left_index, const size_t &right_index,
  const Transforms &transforms,
  const std::vector<
    std::vector<std::vector<std::vector<std::vector<Single_Spin>>>>> &partial);

std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>> zzb_to_yyb(
  const int64_t &parity, const Transforms &transforms,
  const std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
    &zzb);

std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>> zzb_to_xt(
  const int64_t &parity,
  const std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
    &zzb);

std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
lrho_to_xt_radial(const int64_t &parity, const Transforms &transforms,
                  const Single_Spin &spin);

std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
xt_to_ws_radial(
  const Transforms &transforms,
  const std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
    &xt_radial);

void write_derivs(
  const std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
    &derivs,
  boost::filesystem::ofstream &outfile);

void write_output(
  const std::string &output_template, const int64_t &parity,
  const std::array<int64_t, 4> &two_js,
  const std::set<int64_t> &two_js_intermediate, const int64_t &two_j12,
  const int64_t &two_j43, const std::string &delta_12_string,
  const std::string &delta_43_string, const std::string &delta_1_plus_2_string,
  const Q4pm &q4pm, const int64_t &order, const int64_t &lambda,
  const std::set<Coordinate> &coordinates, const int64_t &kept_pole_order,
  const std::vector<std::set<Bigfloat>> &output_poles,
  const size_t &precision_base_2, const int &argc, char *argv[],
  const std::vector<
    std::vector<std::vector<std::vector<std::vector<Single_Spin>>>>> &partial,
  const size_t &num_threads, const bool &debug)
{
  const Bigfloat delta_1_plus_2(delta_1_plus_2_string);
  Transforms transforms(delta_1_plus_2);
  // order==0 is used for radial-only  coordinates
  transforms.insert_if_not_present(0);
  for(auto &p : partial)
    for(auto &two_j : p)
      for(auto &parities : two_j)
        for(auto &left : parities)
          for(auto &right : left)
            {
              const int64_t order(right.numerator.size() - 1 + parity);
              transforms.insert_if_not_present(order);
            }
  const size_t two_j_size(two_js_intermediate.size());

  std::atomic_size_t global_index(0);
  std::list<std::thread> threads;
  for(size_t thread_rank = 0; thread_rank < num_threads; ++thread_rank)
    {
      threads.emplace_back(
        [&](const size_t &thread_rank) {
          try
            {
              size_t two_j_index(global_index.fetch_add(1));
              while(two_j_index < two_j_size)
                {
                  int64_t two_j_intermediate(
                    *std::next(two_js_intermediate.begin(), two_j_index));
                  boost::filesystem::path outfile_path(
                    fmt::format(output_template, (two_j_intermediate / 2.0)));
                  if(debug)
                    {
                      std::stringstream ss;
                      ss << "write_output: " << thread_rank << " "
                         << two_j_index << " " << outfile_path << "\n";
                      std::cout << ss.str() << std::flush;
                    }
                  if(!outfile_path.parent_path().empty())
                    {
                      boost::filesystem::create_directories(
                        outfile_path.parent_path());
                    }
                  boost::filesystem::ofstream outfile(outfile_path);
                  outfile.precision(
                    boost::multiprecision::mpf_float::default_precision());

                  write_header(outfile, output_template, two_js,
                               two_j_intermediate, two_js_intermediate,
                               two_j12, two_j43, delta_12_string,
                               delta_43_string, delta_1_plus_2_string, q4pm,
                               order, lambda, coordinates, kept_pole_order,
                               output_poles.at(two_j_index), precision_base_2,
                               argc, argv);
                  outfile << "  \"derivs\":\n  [\n";

                  const size_t parity_size(
                    partial.front()[two_j_index].size());
                  for(size_t parity_index(0); parity_index != parity_size;
                      ++parity_index)
                    {
                      outfile << "    [\n";
                      const size_t left_size(
                        partial.front()[two_j_index][parity_index].size());
                      for(size_t left_index(0); left_index != left_size;
                          ++left_index)
                        {
                          outfile << "      [\n";
                          const size_t right_size(
                            partial
                              .front()[two_j_index][parity_index][left_index]
                              .size());
                          for(size_t right_index(0); right_index != right_size;
                              ++right_index)
                            {
                              const std::vector<std::vector<
                                boost::math::tools::polynomial<Bigfloat>>>
                                zzb([&]() {
                                  if(coordinates.find(Coordinate::zzb)
                                       != coordinates.end()
                                     || coordinates.find(Coordinate::xt)
                                          != coordinates.end()
                                     || coordinates.find(Coordinate::yyb)
                                          != coordinates.end()
                                     || coordinates.find(Coordinate::ws)
                                          != coordinates.end())
                                    {
                                      return rho_rhob_to_zzb(
                                        parity, two_j_index, parity_index,
                                        left_index, right_index, transforms,
                                        partial);
                                    }
                                  return std::vector<std::vector<
                                    boost::math::tools::polynomial<Bigfloat>>>();
                                }()),
                                yyb((coordinates.find(Coordinate::yyb)
                                       != coordinates.end()
                                     || coordinates.find(Coordinate::ws)
                                          != coordinates.end())
                                      ? zzb_to_yyb(parity, transforms, zzb)
                                      : std::vector<std::vector<
                                          boost::math::tools::polynomial<
                                            Bigfloat>>>()),
                                xt_radial(
                                  (coordinates.find(Coordinate::xt_radial)
                                     != coordinates.end()
                                   || coordinates.find(Coordinate::ws_radial)
                                        != coordinates.end())
                                    ? lrho_to_xt_radial(
                                        parity, transforms,
                                        partial[parity][two_j_index]
                                               [parity_index][left_index]
                                               [right_index])
                                    : std::vector<std::vector<
                                        boost::math::tools::polynomial<
                                          Bigfloat>>>());

                              outfile << "        [\n";

                              for(auto c(coordinates.begin());
                                  c != coordinates.end(); ++c)
                                {
                                  if(c != coordinates.begin())
                                    {
                                      outfile << ",\n";
                                    }
                                  switch(*c)
                                    {
                                    case Coordinate::zzb:
                                      {
                                        write_derivs(zzb, outfile);
                                        break;
                                      }
                                    case Coordinate::yyb:
                                      {
                                        write_derivs(yyb, outfile);
                                        break;
                                      }
                                    case Coordinate::xt:
                                      {
                                        write_derivs(zzb_to_xt(parity, zzb),
                                                     outfile);
                                        break;
                                      }
                                    case Coordinate::ws:
                                      {
                                        write_derivs(zzb_to_xt(parity, yyb),
                                                     outfile);
                                        break;
                                      }
                                    case Coordinate::xt_radial:
                                      {
                                        write_derivs(xt_radial, outfile);
                                        break;
                                      }
                                    case Coordinate::ws_radial:
                                      {
                                        write_derivs(xt_to_ws_radial(
                                                       transforms, xt_radial),
                                                     outfile);
                                        break;
                                      }
                                    }
                                }
                              outfile << "\n        ]";
                              if(right_index + 1 != right_size)
                                {
                                  outfile << ",\n";
                                }
                            }
                          outfile << "\n      ]";
                          if(left_index + 1 != left_size)
                            {
                              outfile << ",\n";
                            }
                        }
                      outfile << "\n    ]";
                      if(parity_index + 1 != parity_size)
                        {
                          outfile << ",\n";
                        }
                    }
                  outfile << "\n  ]";
                  outfile << "\n}\n";
                  two_j_index = global_index.fetch_add(1);
                }
            }
          catch(fmt::format_error &e)
            {
              std::stringstream ss;
              ss << "Formatting error on thread " << thread_rank
                 << " for the template '" << output_template
                 << "': " << e.what() << "\n";
              std::cerr << ss.str();
              exit(1);
            }
          catch(std::exception &e)
            {
              std::stringstream ss;
              ss << "Error on thread " << thread_rank << ": " << e.what()
                 << "\n";
              std::cerr << ss.str();
              exit(1);
            }
          catch(...)
            {
              std::stringstream ss;
              ss << "Unknown error on thread " << thread_rank << "\n";
              std::cerr << ss.str();
              exit(1);
            }
        },
        thread_rank);
    }
  for(auto &thread : threads)
    {
      thread.join();
    }
}
