#include "../../../Eigen.hxx"

Eigen_Matrix rho_z_transform(const int64_t &order)
{
  Eigen_Matrix z_rho(order + 1, order + 1);
  for(int64_t p(0); p <= order; ++p)
    for(int64_t n(0); n <= order; ++n)
      {
        if(p >= n)
          {
            Bigfloat sum(0);
            for(int64_t k(0); k <= n; ++k)
              {
                sum += binomial_coefficient(n, k) * pow(Bigfloat(0.5), n)
                       * (k % 2 == 0 ? 1 : -1)
                       * pow(4 * sqrt(Bigfloat(2)), n - k)
                       * binomial_coefficient(-2 * n, p - k - n)
                       * pow(4 - 2 * sqrt(Bigfloat(2)), -n - p + k);
              }
            z_rho(p, n) = (factorial_slow(p) / factorial_slow(n)) * sum;
          }
        else
          {
            z_rho(p, n) = 0;
          }
      }
  return z_rho.inverse();
}
