#include "../Bigfloat.hxx"

#include <boost/math/tools/polynomial.hpp>

#include <vector>

std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>> zzb_to_xt(
  const int64_t &parity,
  const std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>> &zzb)
{
  const int64_t order(zzb.size() - 1);
  std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>> result(
    order - parity + 1);
  for(int64_t n(0); n < int64_t(result.size()); ++n)
    {
      int64_t bar_size((order - n - parity) / 2 + 1);
      result[n].resize(bar_size);
      int64_t m(parity);
      for(auto &r : result[n])
        {
          for(int64_t k(0); k <= n; ++k)
            {
              for(int64_t l(0); l <= m; ++l)
                {
                  const int64_t i(n + m - k - l), j(k + l);

                  r += (l % 2 == 0 ? 1 : -1)
                       * (factorial_slow((m - parity) / 2) / factorial_slow(m))
                       * binomial_coefficient(n, k)
                       * binomial_coefficient(m, l)
                       * (i >= j
                            ? zzb.at(i).at(j)
                            : ((parity % 2 == 0 ? 1 : -1) * zzb.at(j).at(i)));
                }
            }
          m += 2;
        }
    }
  return result;
}
