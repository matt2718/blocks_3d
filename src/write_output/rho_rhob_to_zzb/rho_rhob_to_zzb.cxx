#include "../congruence_transform.hxx"
#include "../../Single_Spin.hxx"

std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
lrho_lrhob_to_rho_rhob(
  const int64_t &parity, const size_t &two_j_index, const size_t &parity_index,
  const size_t &left_index, const size_t &right_index,
  const Transforms &transforms,
  const std::vector<
    std::vector<std::vector<std::vector<std::vector<Single_Spin>>>>> &partial);

std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
rho_rhob_to_zzb(
  const int64_t &parity, const size_t &two_j_index, const size_t &parity_index,
  const size_t &left_index, const size_t &right_index,
  const Transforms &transforms,
  const std::vector<
    std::vector<std::vector<std::vector<std::vector<Single_Spin>>>>> &partial)
{
  std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>> zzb(
    congruence_transform(
      parity, transforms, Transforms::Index::rho_z,
      lrho_lrhob_to_rho_rhob(parity, two_j_index, parity_index, left_index,
                             right_index, transforms, partial)));

  if(transforms.needs_prefactor())
    {
      return congruence_transform(parity, transforms,
                                  Transforms::Index::power_prefactor, zzb);
    }
  return zzb;
}
