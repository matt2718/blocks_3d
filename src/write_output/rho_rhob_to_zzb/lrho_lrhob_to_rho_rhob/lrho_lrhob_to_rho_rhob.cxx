#include "../../congruence_transform.hxx"
#include "../../Transforms.hxx"
#include "../../../Single_Spin.hxx"

#include <map>

std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
lambda_lr_to_lrho_lrhob(
  const int64_t &parity, const size_t &two_j_index, const size_t &parity_index,
  const size_t &left_index, const size_t &right_index,
  const std::vector<
    std::vector<std::vector<std::vector<std::vector<Single_Spin>>>>> &partial);

std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
lrho_lrhob_to_rho_rhob(
  const int64_t &parity, const size_t &two_j_index, const size_t &parity_index,
  const size_t &left_index, const size_t &right_index,
  const Transforms &transforms,
  const std::vector<
    std::vector<std::vector<std::vector<std::vector<Single_Spin>>>>> &partial)
{
  return congruence_transform(parity, transforms, Transforms::Index::stirling,
                              lambda_lr_to_lrho_lrhob(parity, two_j_index,
                                                      parity_index, left_index,
                                                      right_index, partial));
}
