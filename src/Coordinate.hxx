#pragma once

#include <boost/algorithm/string.hpp>

#include <iostream>

enum class Coordinate
{
  zzb,
  yyb,
  xt,
  ws,
  xt_radial,
  ws_radial
};

inline std::string to_string(const Coordinate &c)
{
  switch(c)
    {
    case Coordinate::zzb: return "zzb";
    case Coordinate::yyb: return "yyb";
    case Coordinate::xt: return "xt";
    case Coordinate::ws: return "ws";
    case Coordinate::xt_radial: return "xt_radial";
    case Coordinate::ws_radial: return "ws_radial";
    default:
      throw std::runtime_error("Unhandled case in to_string(Coordinate)");
    }
}

inline Coordinate to_Coordinate(const std::string &s)
{
  std::string s_copy(boost::algorithm::trim_copy(s));
  boost::algorithm::to_lower(s_copy);
  if(s_copy == "zzb")
    {
      return Coordinate::zzb;
    }
  else if(s_copy == "yyb")
    {
      return Coordinate::yyb;
    }
  else if(s_copy == "xt")
    {
      return Coordinate::xt;
    }
  else if(s_copy == "ws")
    {
      return Coordinate::ws;
    }
  else if(s_copy == "xt_radial")
    {
      return Coordinate::xt_radial;
    }
  else if(s_copy == "ws_radial")
    {
      return Coordinate::ws_radial;
    }
  else
    {
      throw std::runtime_error(
        "Invalid entry for coordinates.  Only 'zzb', 'yyb', 'xt', 'ws', "
        "'xt_radial', and/or 'ws_radial' are allowed, but found: '"
        + s + "'");
    }
}

inline std::ostream &operator<<(std::ostream &os, const Coordinate &c)
{
  return os << to_string(c);
}
