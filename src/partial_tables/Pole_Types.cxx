#include "Pole_Types.hxx"
#include "bosonic.hxx"

#include <vector>
#include <array>

namespace
{
  int64_t pole_range_k(const Pole::Type &type, const int64_t &two_j,
                       const int64_t &two_j12, const int64_t &two_j43,
                       const int64_t &order)
  {
    switch(type)
      {
      case Pole::Type::I: return order;
      case Pole::Type::II: return std::min(two_j / 2, order);
      case Pole::Type::III:
        if(bosonic(two_j))
          {
            return order / 2;
          }
        else
          {
            return std::min(
              two_j / 2,
              std::min(two_j12 / 2, std::min(two_j43 / 2, order / 2)));
          }
      case Pole::Type::IV:
        if(bosonic(two_j))
          {
            return std::min(
              two_j / 2,
              std::min(two_j12 / 2, std::min(two_j43 / 2, (order + 1) / 2)));
          }
        else
          {
            return (order + 1) / 2;
          }
      }
    throw std::runtime_error(
      "INTERNAL_ERROR: Reached the end of pole_range_k");
  }
}

// The types of poles that a block with 2*j=two_j has
// TODO: double check the upper bounds on these families
const std::vector<Pole> &
Pole_Types::calculate(const int64_t &two_j, const int64_t &two_j12,
                      const int64_t &two_j43, const int64_t &order)
{
  std::tuple<int64_t, int64_t, int64_t, int64_t> key(two_j, two_j12, two_j43,
                                                     order);
  auto iterator((cache.emplace(key, std::vector<Pole>())).first);
  std::vector<Pole> &result(iterator->second);
  for(auto &type : std::array<Pole::Type, 4>(
        {Pole::Type::I, Pole::Type::II, Pole::Type::III, Pole::Type::IV}))
    {
      for(int64_t k(1);
          k <= pole_range_k(type, two_j, two_j12, two_j43, order); ++k)
        {
          result.emplace_back(type, k);
        }
    }
  return iterator->second;
}
