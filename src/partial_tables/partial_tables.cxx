#include "Cache.hxx"
#include "../Single_Spin.hxx"
#include "../Q4pm.hxx"

#include <boost/filesystem.hpp>

#include <set>
#include <thread>
#include <atomic>

std::vector<std::vector<std::vector<std::vector<Single_Spin>>>>
log_r_lambda_deriv(const std::array<int64_t, 4> &two_js,
                   const Bigfloat &delta_12, const Bigfloat &delta_43,
                   const int64_t &two_j12, const int64_t &two_j43,
                   const Q4pm &q4pm,
                   const std::set<int64_t> &two_js_intermediate,
                   const int64_t &r_derivs, const int64_t &n,
                   const int64_t &kept_pole_order, const int64_t &order,
                   std::vector<std::set<Bigfloat>> &output_poles,
                   Cache &cache);

std::vector<std::vector<std::vector<std::vector<std::vector<Single_Spin>>>>>
partial_tables(const std::array<int64_t, 4> &two_js, const Bigfloat &delta_12,
               const Bigfloat &delta_43, const int64_t &two_j12,
               const int64_t &two_j43, const Q4pm &q4pm,
               const std::set<int64_t> &two_js_intermediate,
               const int64_t &lambda, const int64_t &kept_pole_order,
               const int64_t &order, const size_t &num_threads,
               const bool &debug,
               std::vector<std::set<Bigfloat>> &output_poles)
{
  std::vector<int64_t> n_array;
  for(int64_t n = (1 - q4pm.i) / 2; n <= lambda; n += 2)
    {
      n_array.push_back(n);
    }
  std::vector<std::vector<std::vector<std::vector<std::vector<Single_Spin>>>>>
    result(n_array.size());

  std::vector<std::vector<std::set<Bigfloat>>> output_pole_threads(
    num_threads);
  std::atomic_size_t global_index(0);
  std::list<std::thread> threads;
  for(size_t thread_rank = 0; thread_rank < num_threads; ++thread_rank)
    {
      threads.emplace_back(
        [&](const size_t &thread_rank) {
          Cache cache(delta_12, delta_43);
          size_t index(global_index.fetch_add(1));
          while(index < result.size())
            {
              const int64_t n(n_array[index]);
              if(debug)
                {
                  std::stringstream ss;
                  ss << "partial: " << thread_rank << " " << index << " " << n
                     << "\n";
                  std::cout << ss.str() << std::flush;
                }
              result[index] = log_r_lambda_deriv(
                two_js, delta_12, delta_43, two_j12, two_j43, q4pm,
                two_js_intermediate, lambda - n, n, kept_pole_order, order,
                output_pole_threads[thread_rank], cache);
              index = global_index.fetch_add(1);
            }
        },
        thread_rank);
    }
  for(auto &thread : threads)
    {
      thread.join();
    }

  for(auto &poles_per_thread : output_pole_threads)
    {
      if(output_poles.size() < poles_per_thread.size())
        {
          output_poles.resize(poles_per_thread.size());
        }
      for(size_t two_j_index(0); two_j_index != poles_per_thread.size();
          ++two_j_index)
        {
          output_poles.at(two_j_index)
            .insert(poles_per_thread.at(two_j_index).begin(),
                    poles_per_thread.at(two_j_index).end());
        }
    }
  return result;
}
