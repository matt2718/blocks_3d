#include "Delta_Fraction.hxx"
#include "../Cache.hxx"
#include "../../Single_Spin.hxx"
#include "../../Q4pm.hxx"

#include <boost/filesystem.hpp>

#include <set>

std::vector<std::vector<std::vector<std::vector<std::vector<Delta_Fraction>>>>>
conformal_block_derivatives(const std::array<int64_t, 4> &two_js,
                            const Bigfloat &delta_12, const int64_t &two_j12,
                            const Bigfloat &delta_43, const int64_t &two_j43,
                            const Q4pm &q4pm,
                            const std::set<int64_t> &two_js_intermediate,
                            const int64_t &order, const int64_t &n,
                            const int64_t &r_derivs, Cache &cache);

Single_Spin shift_block(const std::vector<Delta_Fraction> &block,
                        const int64_t &two_j, const int64_t &two_j12,
                        const int64_t &two_j43, const int64_t &kept_pole_order,
                        std::set<Bigfloat> &output_poles, Cache &cache);

std::vector<std::vector<std::vector<std::vector<Single_Spin>>>>
log_r_lambda_deriv(const std::array<int64_t, 4> &two_js,
                   const Bigfloat &delta_12, const Bigfloat &delta_43,
                   const int64_t &two_j12, const int64_t &two_j43,
                   const Q4pm &q4pm,
                   const std::set<int64_t> &two_js_intermediate,
                   const int64_t &r_derivs, const int64_t &n,
                   const int64_t &kept_pole_order, const int64_t &order,
                   std::vector<std::set<Bigfloat>> &output_poles, Cache &cache)
{
  std::vector<std::vector<std::vector<std::vector<std::vector<Delta_Fraction>>>>>
    blocks(conformal_block_derivatives(two_js, delta_12, two_j12, delta_43,
                                       two_j43, q4pm, two_js_intermediate,
                                       order, n, r_derivs, cache));

  std::vector<std::vector<std::vector<std::vector<Single_Spin>>>> result;
  result.reserve(two_js_intermediate.size());
  if(output_poles.size() < two_js_intermediate.size())
    {
      output_poles.resize(two_js_intermediate.size());
    }
  // blocks only has entries for the elements of
  // two_js_intermediate, which may skip values of two_j.
  size_t two_j_index(0);
  for(auto &two_j : two_js_intermediate)
    {
      result.emplace_back();
      auto &two_js(result.back());
      for(auto &parity : blocks.at(two_j_index))
        {
          two_js.emplace_back();
          auto &parities(two_js.back());
          for(auto &left : parity)
            {
              parities.emplace_back();
              auto &lefts(parities.back());
              for(auto &right : left)
                {
                  lefts.push_back(shift_block(
                    right, two_j, two_j12, two_j43, kept_pole_order,
                    output_poles.at(two_j_index), cache));
                }
            }
        }
      ++two_j_index;
    }
  return result;
}
