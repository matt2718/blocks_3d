#pragma once

#include "../../Bigfloat.hxx"
#include "../../ostream_pair.hxx"
#include "../../ostream_vector.hxx"

#include <boost/math/tools/polynomial.hpp>

#include <utility>
#include <vector>

struct Delta_Fraction
{
  boost::math::tools::polynomial<Bigfloat> polynomial;
  std::vector<std::pair<Bigfloat, Bigfloat>> residues;
};

inline std::ostream &operator<<(std::ostream &os, const Delta_Fraction &fraction)
{
  os << "{\"polynomial\": " << fraction.polynomial << ", "
     << "\"residues\": " << fraction.residues << "}";
  return os;
}
