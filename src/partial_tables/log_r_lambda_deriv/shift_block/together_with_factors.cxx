#include "../Delta_Fraction.hxx"
#include "../../Cache.hxx"

boost::math::tools::polynomial<Bigfloat> together_with_factors(
  const std::vector<Bigfloat> &all_kept_first,
  const std::vector<Bigfloat> &all_kept_second,
  const boost::math::tools::polynomial<Bigfloat> &polynomial, Cache &cache)
{
  boost::math::tools::polynomial<Bigfloat> product(
    polynomial * cache.pole_product.eval(all_kept_first));

  auto &complement_product(cache.pole_complement_product.eval(all_kept_first));
  boost::math::tools::polynomial<Bigfloat> sum;
  for(size_t index(0); index < all_kept_second.size(); ++index)
    {
      sum += all_kept_second[index] * complement_product[index];
    }
  return product + sum;
}
