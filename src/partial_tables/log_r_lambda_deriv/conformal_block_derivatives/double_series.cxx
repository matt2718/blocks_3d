#include "../../../Bigfloat.hxx"

#include <vector>

std::vector<std::vector<Bigfloat>>
double_series(const int64_t &order, const std::vector<Bigfloat> &binomial_a,
              const std::vector<Bigfloat> &binomial_ab,
              const std::vector<Bigfloat> &binomial_b,
              const std::vector<Bigfloat> &binomial_bb)
{
  std::vector<std::vector<Bigfloat>> result;
  Bigfloat sum, temp;
  std::vector<Bigfloat> m2_terms, m3_terms;
  for(int64_t n = 0; n <= order; ++n)
    {
      result.emplace_back();
      auto &vec(result.back());
      vec.reserve(n + 1);
      for(int64_t j = 0; j <= n; ++j)
        {
          sum = 0;

          m3_terms.resize(j + 1);
          for(int64_t m3 = 0; m3 <= j; ++m3)
            {
              m3_terms[m3] = binomial_a[j - m3] * binomial_b[m3];
            }

          m2_terms.resize(n - j + 1);
          for(int64_t m2 = 0; m2 <= n - j; ++m2)
            {
              m2_terms[m2] = binomial_ab[m2] * binomial_bb[n - j - m2];
            }

          for(int64_t m3 = 0; m3 <= j; ++m3)
            {
              for(int64_t m2 = 0; m2 <= n - j; ++m2)
                {
                  // Some complication to avoid creating and
                  // destroying temporary Bigfloat's.
                  temp=m3_terms[m3];
                  temp*=m2_terms[m2];
                  if((n - j - m3 - m2) % 2 == 0)
                    {
                      sum+=temp;
                    }
                  else
                    {
                      sum-=temp;
                    }
                }
            }
          vec.emplace_back(std::move(sum));
        }
    }
  return result;
}
