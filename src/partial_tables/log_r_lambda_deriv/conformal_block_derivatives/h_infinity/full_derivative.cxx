#include "F_Derivatives.hxx"

Bigfloat
full_derivative(const std::array<int64_t, 4> &two_js, const int64_t &r_order,
                const int64_t &two_j, const SO3 &left_SO3,
                const SO3 &right_SO3, const int64_t &n,
                const std::vector<std::vector<Bigfloat>> &prefactor,
                F_Derivatives &f_derivatives)
{
  std::vector<Bigfloat> f_derivs(
    f_derivatives.eval(two_js, two_j, left_SO3, right_SO3));

  Bigfloat result(0);
  for(int64_t k(0); k <= n; ++k)
    {
      result += binomial_coefficient(n, k) * f_derivs[k]
                * prefactor[n - k][r_order];
    }
  return result;
}
