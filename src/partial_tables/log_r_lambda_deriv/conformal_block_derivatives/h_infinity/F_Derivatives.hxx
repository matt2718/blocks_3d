#pragma once

#include "SO3.hxx"
#include "../../../Cache.hxx"

#include <map>
#include <tuple>

struct F_Derivatives
{
  const std::array<int64_t, 4> &q4;
  const int64_t &n;
  Cache &cache;

  F_Derivatives(const std::array<int64_t, 4> &Q4, const int64_t &N,
                Cache &cache_ref)
      : q4(Q4), n(N), cache(cache_ref)
  {}

  std::map<std::tuple<std::array<int64_t, 4>, int64_t, SO3, SO3>,
           std::vector<Bigfloat>>
    internal_cache;
  const std::vector<Bigfloat> &
  eval(const std::array<int64_t, 4> &two_js, const int64_t &two_j,
       const SO3 &left_SO3, const SO3 &right_SO3)
  {
    const std::tuple<std::array<int64_t, 4>, int64_t, SO3, SO3> key(
      two_js, two_j, left_SO3, right_SO3);
    auto element(internal_cache.find(key));
    if(element != internal_cache.end())
      {
        return element->second;
      }
    else
      {
        return calculate(two_js, two_j, left_SO3, right_SO3);
      }
  }
  const std::vector<Bigfloat> &
  calculate(const std::array<int64_t, 4> &two_js, const int64_t &two_j,
            const SO3 &left_SO3, const SO3 &right_SO3);
};
