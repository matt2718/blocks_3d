#include "F_Derivatives.hxx"
#include "../two_j_values.hxx"
#include "../../../../so3_tensor_parity.hxx"
#include "../../../../compute_pseudo_parity_types.hxx"

std::vector<std::vector<Bigfloat>>
prefactor_derivatives(const int64_t &order,
                      const std::vector<std::vector<Bigfloat>> &series,
                      const Bigfloat &c, const int64_t &k_max);

Bigfloat
full_derivative(const std::array<int64_t, 4> &two_js, const int64_t &r_order,
                const int64_t &two_j, const SO3 &left_SO3,
                const SO3 &right_SO3, const int64_t &n,
                const std::vector<std::vector<Bigfloat>> &prefactor,
                F_Derivatives &f_derivatives);

std::vector<std::vector<std::vector<std::vector<std::vector<Bigfloat>>>>>
h_infinity(const std::array<int64_t, 4> &two_js,
           const std::vector<std::vector<Bigfloat>> &series,
           const int64_t &two_j12, const int64_t &two_j43,
           const std::array<int64_t, 4> &q4, const int64_t &two_j_max,
           const int64_t &order, const int64_t &n, Cache &cache)
{
  const Bigfloat c((q4[0] + q4[1]) / 4.0);
  const std::vector<std::vector<Bigfloat>> prefactor(
    prefactor_derivatives(order, series, c, n));
  F_Derivatives f_derivatives(q4, n, cache);

  std::array<std::array<int64_t, 2>, 2> pseudo_parity_types(
    compute_pseudo_parity_types(two_js, q4, {two_js[0], two_js[1], two_j12},
                                {two_js[3], two_js[2], two_j43}));

  std::vector<std::vector<std::vector<std::vector<std::vector<Bigfloat>>>>>
    result;
  for(int64_t pp_index(0); pp_index != 2; ++pp_index)
    {
      result.emplace_back();
      auto &pp_back(result.back());
      for(int64_t r_order(0); r_order <= order; ++r_order)
        {
          pp_back.emplace_back();
          auto &r_back(pp_back.back());
          for(auto &two_j : two_j_values(r_order, order, two_j12, two_j_max))
            {
              r_back.emplace_back();
              auto &two_j_back(r_back.back());
              for(auto &two_j120 : so3_tensor_parity(
                    pseudo_parity_types[pp_index][0], two_j12, two_j))
                {
                  two_j_back.emplace_back();
                  auto &two_j120_back(two_j_back.back());
                  for(auto &two_j430 : so3_tensor_parity(
                        pseudo_parity_types[pp_index][1], two_j43, two_j))
                    {
                      two_j120_back.push_back(full_derivative(
                        two_js, r_order, two_j, SO3(two_j12, two_j120),
                        SO3(two_j43, two_j430), n, prefactor, f_derivatives));
                    }
                }
            }
        }
    }
  return result;
}
