// tildeWignerCoefficient[2j,2m,2mp,n] computes the n-th derivative of \tilde
// d^j_{m,mp}(-\theta) in \lambda=i \theta at \lambda=0.

// \tilde d^j_{m,mp}(-\theta) is defined as

// I^(mp-m)WignerD[{j,m,mp},-theta]

#include "../../../../../../../../../Bigfloat.hxx"
#include "../../../../../../../../Factorial.hxx"
#include "../../../../../../../../Sinh_Power_Derivative.hxx"

Bigfloat coefficient(const int64_t &two_j, const int64_t &two_m_out,
                     const int64_t &two_mp_out, const int64_t &Nn,
                     Factorial &factorial);

// Valid input always has
// two_j, Nn >= 0
// two_j-two_mout, two_j-two_mpout are even
// abs(two_mout), abs(two_mpout) <= two_j

Bigfloat tilde_wigner_derivative(const int64_t &two_j, const int64_t &two_m,
                                 const int64_t &two_mp, const int64_t &Nn,
                                 Factorial &factorial,
                                 Sinh_Power_Derivative &sinh_power_derivative)
{
  if((Nn < std::abs(two_m - two_mp) / 2)
     || (Nn - std::abs(two_m - two_mp) / 2) % 2 == 1)
    {
      return 0;
    }

  Bigfloat result(0);

  for(int64_t n = 0; n <= (Nn - std::abs(two_m - two_mp) / 2) / 2; ++n)
    {
      result += sinh_power_derivative.eval(
                  2 * n + std::abs(two_m - two_mp) / 2,
                  (Nn - 2 * n - std::abs(two_m - two_mp) / 2) / 2)
                * coefficient(two_j, two_m, two_mp, n, factorial);
    }
  return result;
}
