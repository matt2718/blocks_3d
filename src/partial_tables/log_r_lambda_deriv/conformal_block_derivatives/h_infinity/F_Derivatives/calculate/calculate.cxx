#include "../../F_Derivatives.hxx"

Bigfloat
f_SO3_basis_derivative(const std::array<int64_t, 4> &two_js,
                       const int64_t &two_j, const SO3 &left_SO3,
                       const SO3 &right_SO3, const std::array<int64_t, 4> &q4,
                       const int64_t &n, Cache &cache);

const std::vector<Bigfloat> &
F_Derivatives::calculate(const std::array<int64_t, 4> &two_js,
                         const int64_t &two_j, const SO3 &left_SO3,
                         const SO3 &right_SO3)
{
  const std::tuple<std::array<int64_t, 4>, int64_t, SO3, SO3> key(
    two_js, two_j, left_SO3, right_SO3);
  auto iterator((internal_cache.emplace(key, std::vector<Bigfloat>())).first);
  std::vector<Bigfloat> &result(iterator->second);

  result.reserve(n + 1);
  for(int64_t k = 0; k <= n; ++k)
    {
      result.emplace_back(f_SO3_basis_derivative(
        two_js, two_j, left_SO3, right_SO3, q4, k, cache));
    }
  return iterator->second;
}
