#include "../../../Bigfloat.hxx"
#include "../../Factorial.hxx"

Bigfloat clebsch_gordan(const int64_t &two_j1, const int64_t &two_m1,
                        const int64_t &two_j2, const int64_t &two_m2,
                        const int64_t &two_J, const int64_t &two_M,
                        Factorial &factorial)
{
  Bigfloat result(0);
  if((two_J - two_j1 - two_j2) % 2
       == 0 // Also implies all cases of two_J +- two_j1 +- two_j2 are even
     && std::abs(two_j1 - two_j2) <= two_J && two_J <= two_j1 + two_j2
     && (two_J - two_M) % 2 == 0 // Also implies two_J + two_M is even
     && (two_j1 - two_m1) % 2 == 0 && (two_j2 - two_m2) % 2 == 0
     && std::abs(two_M) <= two_J && std::abs(two_m1) <= two_j1
     && std::abs(two_m2) <= two_j2 && (two_M == two_m1 + two_m2))
    {
      result
        = sqrt((two_J + 1) * factorial.eval((two_J + two_j1 - two_j2) / 2)
               * factorial.eval((two_J - two_j1 + two_j2) / 2)
               * factorial.eval((-two_J + two_j1 + two_j2) / 2)
               * factorial.inverse_eval((2 + two_J + two_j1 + two_j2) / 2))
          * sqrt(factorial.eval((two_J + two_M) / 2)
                 * factorial.eval((two_J - two_M) / 2)
                 * factorial.eval((two_j1 + two_m1) / 2)
                 * factorial.eval((two_j1 - two_m1) / 2)
                 * factorial.eval((two_j2 + two_m2) / 2)
                 * factorial.eval((two_j2 - two_m2) / 2));
      Bigfloat sum(0);
      for(int64_t k(std::max(int64_t(0), std::max(-(two_J - two_j2 + two_m1),
                                                  -(two_J - two_j1 - two_m2)))
                    / 2);
          k <= std::min(two_j1 + two_j2 - two_J,
                        std::min(two_j1 - two_m1, two_j2 + two_m2))
                 / 2;
          ++k)
        {
          sum += (k % 2 == 0 ? 1 : -1)
                 / (factorial.eval(k)
                    * factorial.eval((two_j1 + two_j2 - two_J) / 2 - k)
                    * factorial.eval((two_j1 - two_m1) / 2 - k)
                    * factorial.eval((two_j2 + two_m2) / 2 - k)
                    * factorial.eval((two_J - two_j2 + two_m1) / 2 + k)
                    * factorial.eval((two_J - two_j1 - two_m2) / 2 + k));
        }
      result *= sum;
    }
  return result;
}
