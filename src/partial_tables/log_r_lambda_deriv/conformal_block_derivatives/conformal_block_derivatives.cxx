#include "../Delta_Fraction.hxx"
#include "../../Cache.hxx"
#include "../../../Q4pm.hxx"
#include "../../../so3_tensor_parity.hxx"
#include "../../../compute_pseudo_parity_types.hxx"

#include <boost/math/tools/polynomial.hpp>

#include <numeric>
#include <set>

void fill_binomials(const Bigfloat &delta_12, const Bigfloat &delta_43,
                    const std::array<int64_t, 4> &q4, const int64_t &order,
                    std::vector<Bigfloat> &binomial_a,
                    std::vector<Bigfloat> &binomial_ab,
                    std::vector<Bigfloat> &binomial_b,
                    std::vector<Bigfloat> &binomial_bb);

std::vector<std::vector<Bigfloat>>
double_series(const int64_t &order, const std::vector<Bigfloat> &binomial_a,
              const std::vector<Bigfloat> &binomial_ab,
              const std::vector<Bigfloat> &binomial_b,
              const std::vector<Bigfloat> &binomial_bb);

std::vector<std::vector<std::vector<std::vector<std::vector<Bigfloat>>>>>
h_infinity(const std::array<int64_t, 4> &two_js,
           const std::vector<std::vector<Bigfloat>> &series,
           const int64_t &two_j12, const int64_t &two_j43,
           const std::array<int64_t, 4> &q4, const int64_t &two_j_max,
           const int64_t &order, const int64_t &n, Cache &cache);

std::vector<
  std::vector<std::vector<std::vector<std::vector<std::vector<Bigfloat>>>>>>
radial_recursion(
  const std::vector<
    std::vector<std::vector<std::vector<std::vector<Bigfloat>>>>> &h_infinity,
  const int64_t &two_j_max, const int64_t &two_j12, const int64_t &two_j43,
  const std::array<std::array<int64_t, 2>, 2> &pseudo_parities,
  const int64_t &order, Cache &cache);

std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
r_delta_matrices(const int64_t &r_derivs);

std::vector<Delta_Fraction> restore_r_delta(
  const std::vector<Bigfloat> &constants,
  const std::vector<std::pair<Bigfloat, std::vector<Bigfloat>>> &poles,
  const std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
    &r_delta);

template <typename T> T negate(const T &vec)
{
  T result(vec);
  for(auto &v : result)
    {
      v = -v;
    }
  return result;
}

std::vector<std::vector<std::vector<std::vector<std::vector<Delta_Fraction>>>>>
conformal_block_derivatives(const std::array<int64_t, 4> &two_js,
                            const Bigfloat &delta_12, const int64_t &two_j12,
                            const Bigfloat &delta_43, const int64_t &two_j43,
                            const Q4pm &q4pm,
                            const std::set<int64_t> &two_js_intermediate,
                            const int64_t &order, const int64_t &n,
                            const int64_t &r_derivs, Cache &cache)
{
  const std::array<std::array<int64_t, 2>, 2> pseudo_parity_types(
    compute_pseudo_parity_types(two_js, q4pm.vec,
                                {two_js[0], two_js[1], two_j12},
                                {two_js[3], two_js[2], two_j43}));

  std::vector<Bigfloat> binomial_a, binomial_ab, binomial_b, binomial_bb;
  fill_binomials(delta_12, delta_43, q4pm.vec, order, binomial_a, binomial_ab,
                 binomial_b, binomial_bb);

  const std::vector<std::vector<Bigfloat>> series(
    double_series(order, binomial_a, binomial_ab, binomial_b, binomial_bb)),
    series_b(
      double_series(order, binomial_ab, binomial_a, binomial_bb, binomial_b));

  const int64_t two_j_max(*two_js_intermediate.rbegin());
  std::vector<std::vector<std::vector<std::vector<std::vector<Bigfloat>>>>>
    h_inf(h_infinity(two_js, series, two_j12, two_j43, q4pm.vec, two_j_max,
                     order, n, cache));
  const std::vector<std::vector<std::vector<std::vector<std::vector<Bigfloat>>>>>
    h_temp(h_infinity(two_js, series_b, two_j12, two_j43, negate(q4pm.vec),
                      two_j_max, order, n, cache));

  const int64_t factor(
    q4pm.i
    * (std::accumulate(two_js.begin(), two_js.end(), 0) % 4 == 0 ? 1 : -1));

  for(size_t pp_index(0); pp_index != h_inf.size(); ++pp_index)
    for(size_t r_order(0); r_order != h_inf[pp_index].size(); ++r_order)
      for(size_t two_j(0); two_j != h_inf[pp_index][r_order].size(); ++two_j)
        for(size_t two_j120(0);
            two_j120 != h_inf[pp_index][r_order][two_j].size(); ++two_j120)
          for(size_t two_j430(0);
              two_j430 != h_inf[pp_index][r_order][two_j][two_j120].size();
              ++two_j430)
            {
              // The factor of 1/2 was introduced relative to original
              // Mathematica prototype in order to match scalar_blocks
              // This factor is also introduced in Mathematica tests.
              h_inf[pp_index][r_order][two_j][two_j120][two_j430]
                += factor
                   * h_temp[pp_index][r_order][two_j][two_j120][two_j430];
              h_inf[pp_index][r_order][two_j][two_j120][two_j430] *= 0.5;
            }

  const std::vector<
    std::vector<std::vector<std::vector<std::vector<std::vector<Bigfloat>>>>>>
    residues(radial_recursion(h_inf, two_j_max, two_j12, two_j43,
                              pseudo_parity_types, order, cache));

  const Bigfloat two_point_A0(1), r_crossing(3 - 2 * sqrt(Bigfloat(2))),
    term(two_point_A0 * r_crossing);

  std::vector<std::vector<Bigfloat>> coeffs_to_derivs(
    r_derivs + 1, std::vector<Bigfloat>(order + 1));
  for(int64_t r_order(0); r_order != order + 1; ++r_order)
    {
      const Bigfloat pow_term(pow(term, r_order));
      for(size_t k(0); k != coeffs_to_derivs.size(); ++k)
        {
          // TODO: To speed this up, we can compute this iteratively
          // without using pow().
          coeffs_to_derivs[k][r_order]
            = (r_order == 0 && k == 0 ? Bigfloat(1)
                                      : pow(Bigfloat(r_order), k))
              * pow_term;
        }
    }

  const std::vector<std::vector<boost::math::tools::polynomial<Bigfloat>>>
    r_delta(r_delta_matrices(r_derivs));

  std::vector<std::vector<std::vector<std::vector<std::vector<Delta_Fraction>>>>>
    result;
  for(auto &two_j : two_js_intermediate)
    {
      // two_js_intermediate may skip values of two_j, but h_inf and
      // residues both cover the entire range.  So we compute an index
      // for two_j based on its value.
      const size_t two_j_index(two_j / 2);
      result.emplace_back();
      auto &two_j_back(result.back());
      for(size_t pp_index(0); pp_index != 2; ++pp_index)
        {
          two_j_back.emplace_back();
          auto &pp_back(two_j_back.back());
          // TODO: We create these parity vectors just to get the
          // size.  Is this identical to the sizes in h_infinity or
          // residues?
          const size_t left_so3_size(
            so3_tensor_parity(pseudo_parity_types[pp_index][0], two_j, two_j12)
              .size()),
            right_so3_size(so3_tensor_parity(pseudo_parity_types[pp_index][1],
                                             two_j, two_j43)
                             .size());
          for(size_t left_so3_index(0); left_so3_index != left_so3_size;
              ++left_so3_index)
            {
              pp_back.emplace_back();
              auto &left_back(pp_back.back());
              for(size_t right_so3_index(0); right_so3_index != right_so3_size;
                  ++right_so3_index)
                {
                  std::vector<Bigfloat> coeffs_h_inf;
                  coeffs_h_inf.reserve(coeffs_to_derivs.size());
                  for(auto &coeff : coeffs_to_derivs)
                    {
                      coeffs_h_inf.emplace_back(0);
                      auto &h_inf_sum(coeffs_h_inf.back());
                      for(size_t ii(0); ii != coeff.size(); ++ii)
                        {
                          h_inf_sum += coeff[ii]
                                       * h_inf.at(pp_index)
                                           .at(ii)
                                           .at(two_j_index)
                                           .at(left_so3_index)
                                           .at(right_so3_index);
                        }
                    }

                  std::vector<std::pair<Bigfloat, std::vector<Bigfloat>>>
                    coeffs_residues;
                  const std::vector<Pole> &poles(
                    cache.pole_types.eval(two_j, two_j12, two_j43, order));
                  for(size_t pole_index(0); pole_index != poles.size();
                      ++pole_index)
                    {
                      coeffs_residues.emplace_back(
                        poles[pole_index].delta(two_j),
                        std::vector<Bigfloat>());

                      auto &coeffs_residue(coeffs_residues.back().second);
                      coeffs_residue.reserve(coeffs_to_derivs.size());
                      for(auto &coeff : coeffs_to_derivs)
                        {
                          coeffs_residue.emplace_back(0);
                          auto &residue_sum(coeffs_residue.back());
                          for(size_t ii(1); ii != coeff.size(); ++ii)
                            {
                              residue_sum += coeff[ii]
                                             * residues.at(pp_index)
                                                 .at(ii - 1)
                                                 .at(two_j_index)
                                                 .at(pole_index)
                                                 .at(left_so3_index)
                                                 .at(right_so3_index);
                            }
                        }
                    }
                  left_back.emplace_back(
                    restore_r_delta(coeffs_h_inf, coeffs_residues, r_delta));
                }
            }
        }
    }
  return result;
}
