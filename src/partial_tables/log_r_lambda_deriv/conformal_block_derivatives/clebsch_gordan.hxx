#pragma once

#include "../../../Bigfloat.hxx"
#include "../../Factorial.hxx"

Bigfloat clebsch_gordan(const int64_t &two_j1, const int64_t &two_m1,
                        const int64_t &two_j2, const int64_t &two_m2,
                        const int64_t &two_J, const int64_t &two_M,
                        Factorial &factorial);

