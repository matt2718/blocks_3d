#pragma once

#include "../../../../Pole.hxx"

#include <map>
#include <tuple>

namespace
{
  inline Bigfloat d_prefactor(const Pole &pole)
  {
    // Simplifified from 4**(delta + shift)/4**delta.  It turns out
    // that boost's pow(Bigfloat,Bigfloat) is not accurate past 80
    // digits when the second argument is not an integer.
    // pole.shift() is always an integer.
    return pow(Bigfloat(4), pole.shift());
  }
}

struct Q
{
  std::map<std::tuple<Pole, int64_t>, Bigfloat> cache;

  const Bigfloat &eval(const Pole &pole, const int64_t &two_j)
  {
    std::tuple<Pole, int64_t> key(pole, two_j);
    auto element(cache.find(key));
    if(element != cache.end())
      {
        return element->second;
      }
    else
      {
        return calculate(pole, two_j);
      }
  }
  const Bigfloat &calculate(const Pole &pole, const int64_t &two_j)
  {
    std::tuple<Pole, int64_t> key(pole, two_j);
    auto iterator((cache.emplace(key, 0)).first);
    Bigfloat &result(iterator->second);
    switch(pole.type)
      {
      case Pole::Type::I:
      case Pole::Type::II: result = -d_prefactor(pole); break;
      case Pole::Type::III:
        result = -d_prefactor(pole)
                 * Bigfloat(pole.value * (1 + two_j - 2 * pole.value))
                 / Bigfloat(1 + two_j + 2 * pole.value);
        break;
      case Pole::Type::IV:
        result
          = d_prefactor(pole)
            * Bigfloat((2 + two_j - 2 * pole.value) * (2 * pole.value - 1))
            / Bigfloat(2 * two_j + 4 * pole.value);
        break;
      default:
        throw std::runtime_error(
          "Missing case in Q(): "
          + std::to_string(static_cast<int>(pole.type)));
      }
    return iterator->second;
  }
};
