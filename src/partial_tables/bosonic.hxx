#pragma once

#include <cstdint>

inline bool bosonic(const int64_t &two_j) { return two_j % 2 == 0; }
inline bool fermionic(const int64_t &two_j) { return !bosonic(two_j); }
