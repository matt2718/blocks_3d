#include "Pole_Complement_Product.hxx"

const std::vector<boost::math::tools::polynomial<Bigfloat>> &
Pole_Complement_Product::calculate(const std::vector<Bigfloat> &all_kept)
{
  auto iterator(
    (cache.emplace(all_kept,
                   std::vector<boost::math::tools::polynomial<Bigfloat>>()))
      .first);
  auto &result(iterator->second);
  result.reserve(all_kept.size());

  for(size_t index(0); index < all_kept.size(); ++index)
    {
      result.emplace_back(boost::math::tools::polynomial<Bigfloat>({1}));
      auto &element(result.back());
      for(size_t complement_index(0); complement_index < all_kept.size();
          ++complement_index)
        {
          if(complement_index != index)
            {
              element *= boost::math::tools::polynomial<Bigfloat>(
                {-all_kept[complement_index], 1});
            }
        }
    }
  return result;
}
