#pragma once

#include "Pole.hxx"

#include <map>
#include <vector>
#include <tuple>

struct Pole_Types
{
  std::map<std::tuple<int64_t, int64_t, int64_t, int64_t>, std::vector<Pole>>
    cache;

  const std::vector<Pole> &eval(const int64_t &two_j, const int64_t &two_j12,
                                const int64_t &two_j43, const int64_t &order)
  {
    std::tuple<int64_t, int64_t, int64_t, int64_t> key(two_j, two_j12, two_j43,
                                                       order);
    auto element(cache.find(key));
    if(element != cache.end())
      {
        return element->second;
      }
    else
      {
        return calculate(two_j, two_j12, two_j43, order);
      }
  }
  const std::vector<Pole> &
  calculate(const int64_t &two_j, const int64_t &two_j12,
            const int64_t &two_j43, const int64_t &order);
};
