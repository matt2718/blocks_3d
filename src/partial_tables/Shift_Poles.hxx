#pragma once

#include "Keep_LU.hxx"

#include <tuple>

struct Shift_Poles
{
  std::map<std::tuple<std::vector<std::pair<Bigfloat, Bigfloat>>,
                      std::vector<Bigfloat>>,
           std::vector<std::pair<Bigfloat, Bigfloat>>>
    cache;
  const std::vector<std::pair<Bigfloat, Bigfloat>> &
  eval(const std::vector<std::pair<Bigfloat, Bigfloat>> &unprotected_poles,
       const std::vector<Bigfloat> &keep, Keep_LU &keep_lu)
  {
    std::tuple<std::vector<std::pair<Bigfloat, Bigfloat>>,
               std::vector<Bigfloat>>
      key(unprotected_poles, keep);
    auto element(cache.find(key));
    if(element != cache.end())
      {
        return element->second;
      }
    else
      {
        return calculate(unprotected_poles, keep, keep_lu);
      }
  }
  const std::vector<std::pair<Bigfloat, Bigfloat>> &
  calculate(const std::vector<std::pair<Bigfloat, Bigfloat>> &unprotected_poles,
            const std::vector<Bigfloat> &keep, Keep_LU &keep_lu);
};
