#pragma once

#include "../Bigfloat.hxx"

#include <map>
#include <tuple>

struct Sinh_Power_Derivative
{
  std::map<std::tuple<int64_t, int64_t>, Bigfloat> cache;

  // resizing can invalidate references, so pass Bigfloat
  const Bigfloat & eval(const int64_t &n, const int64_t &k)
  {
    std::tuple<int64_t, int64_t> key(n,k);
    auto element(cache.find(key));
    if(element != cache.end())
      {
        return element->second;
      }
    else
      {
        return calculate(n,k);
      }
  }
  const Bigfloat & calculate(const int64_t &n, const int64_t &k)
  {
    std::tuple<int64_t, int64_t> key(n,k);
    auto iterator((cache.emplace(key, Bigfloat(0))).first);
    Bigfloat &result(iterator->second);
    
    if(n == 0)
      {
        if(k==0)
          {
            result=1;
          }
      }
    else
      {
        for(int64_t m = 0; m <= k; ++m)
          {
            // TODO: Is the iterator invalidated if something else
            // gets inserted?
            result += pow(Bigfloat(0.5), 2 * m + 1)
              * eval(n - 1, k - m)
              * binomial_coefficient(2 * k + n, 2 * m + 1);
          }
      }
    return iterator->second;
  }
};
