import os

def options(opt):
    opt.load(['compiler_cxx','gnu_dirs','cxx14','boost','gmp','fmt',
              'eigen','threads'])

def configure(conf):
    conf.load(['compiler_cxx','gnu_dirs','cxx14','boost','gmp','fmt',
               'eigen','threads'])
    conf.load('clang_compilation_database')

def build(bld):
    default_flags=['-Wall', '-Wextra', '-O3']
    # default_flags=['-Wall', '-Wextra', '-O3', '-g']
    # default_flags=['-Wall', '-Wextra', '-g']
    use_packages=['cxx14','boost','gmp','fmt','eigen','threads']
    
    # Main executable
    bld.program(source=['src/main.cxx',
                        'src/handle_input.cxx',
                        'src/compute_pseudo_parity_types.cxx',
                        'src/so3_tensor_parity.cxx',
                        'src/partial_tables/partial_tables.cxx',
                        'src/partial_tables/Clebsch_Gordan.cxx',
                        'src/partial_tables/M_Matrix/calculate/calculate.cxx',
                        'src/partial_tables/M_Matrix/calculate/M/M.cxx',
                        'src/partial_tables/M_Matrix/calculate/M/d_normalized.cxx',
                        'src/partial_tables/Pole_Types.cxx',
                        'src/partial_tables/Shift_Poles.cxx',
                        'src/partial_tables/Keep_LU.cxx',
                        'src/partial_tables/Pole_Product.cxx',
                        'src/partial_tables/Pole_Complement_Product.cxx',
                        'src/partial_tables/log_r_lambda_deriv/log_r_lambda_deriv.cxx',
                        'src/partial_tables/log_r_lambda_deriv/conformal_block_derivatives/conformal_block_derivatives.cxx',
                        'src/partial_tables/log_r_lambda_deriv/conformal_block_derivatives/fill_binomials.cxx',
                        'src/partial_tables/log_r_lambda_deriv/conformal_block_derivatives/double_series.cxx',
                        'src/partial_tables/log_r_lambda_deriv/conformal_block_derivatives/two_j_values.cxx',
                        'src/partial_tables/log_r_lambda_deriv/conformal_block_derivatives/h_infinity/h_infinity.cxx',
                        'src/partial_tables/log_r_lambda_deriv/conformal_block_derivatives/h_infinity/full_derivative.cxx',
                        'src/partial_tables/log_r_lambda_deriv/conformal_block_derivatives/h_infinity/F_Derivatives/calculate/f_SO3_basis_derivative/f_SO3_basis_derivative.cxx',
                        'src/partial_tables/log_r_lambda_deriv/conformal_block_derivatives/h_infinity/F_Derivatives/calculate/f_SO3_basis_derivative/f_W_conversion_matrix.cxx',
                        'src/partial_tables/log_r_lambda_deriv/conformal_block_derivatives/clebsch_gordan.cxx',
                        'src/partial_tables/log_r_lambda_deriv/conformal_block_derivatives/h_infinity/F_Derivatives/calculate/f_SO3_basis_derivative/f_q_basis_derivative/wjm_matrix.cxx',
                        'src/partial_tables/log_r_lambda_deriv/conformal_block_derivatives/h_infinity/F_Derivatives/calculate/f_SO3_basis_derivative/f_q_basis_derivative/f_q_basis_derivative.cxx',
                        'src/partial_tables/log_r_lambda_deriv/conformal_block_derivatives/h_infinity/F_Derivatives/calculate/f_SO3_basis_derivative/f_q_basis_derivative/tilde_wigner_derivative/tilde_wigner_derivative.cxx',
                        'src/partial_tables/log_r_lambda_deriv/conformal_block_derivatives/h_infinity/F_Derivatives/calculate/f_SO3_basis_derivative/f_q_basis_derivative/tilde_wigner_derivative/coefficient.cxx',
                        'src/partial_tables/log_r_lambda_deriv/conformal_block_derivatives/h_infinity/prefactor_derivatives/prefactor_derivatives.cxx',
                        'src/partial_tables/log_r_lambda_deriv/conformal_block_derivatives/h_infinity/F_Derivatives/calculate/calculate.cxx',
                        'src/partial_tables/log_r_lambda_deriv/conformal_block_derivatives/radial_recursion/radial_recursion.cxx',
                        'src/partial_tables/log_r_lambda_deriv/conformal_block_derivatives/radial_recursion/zero_init_residues.cxx',
                        'src/partial_tables/log_r_lambda_deriv/conformal_block_derivatives/radial_recursion/fill_residues/fill_residues.cxx',
                        'src/partial_tables/log_r_lambda_deriv/conformal_block_derivatives/radial_recursion/fill_residues/set_residue.cxx',
                        'src/partial_tables/log_r_lambda_deriv/conformal_block_derivatives/restore_r_delta.cxx',
                        'src/partial_tables/log_r_lambda_deriv/conformal_block_derivatives/r_delta_matrices.cxx',
                        'src/partial_tables/log_r_lambda_deriv/shift_block/shift_block.cxx',
                        'src/partial_tables/log_r_lambda_deriv/shift_block/shift_fraction.cxx',
                        'src/partial_tables/log_r_lambda_deriv/shift_block/together_with_factors.cxx',
                        'src/write_output/write_output.cxx',
                        'src/write_output/write_header.cxx',
                        'src/write_output/write_derivs.cxx',
                        'src/write_output/congruence_transform.cxx',
                        'src/write_output/Transforms/insert_if_not_present/insert_if_not_present.cxx',
                        'src/write_output/Transforms/insert_if_not_present/stirling_transform.cxx',
                        'src/write_output/Transforms/insert_if_not_present/z_y_transform.cxx',
                        'src/write_output/Transforms/insert_if_not_present/rho_z_transform.cxx',
                        'src/write_output/Transforms/insert_if_not_present/power_prefactor.cxx',
                        'src/write_output/Transforms/insert_if_not_present/one_minus_power_prefactor.cxx',
                        'src/write_output/rho_rhob_to_zzb/rho_rhob_to_zzb.cxx',
                        'src/write_output/rho_rhob_to_zzb/lrho_lrhob_to_rho_rhob/lrho_lrhob_to_rho_rhob.cxx',
                        'src/write_output/rho_rhob_to_zzb/lrho_lrhob_to_rho_rhob/lambda_lr_to_lrho_lrhob.cxx',
                        'src/write_output/zzb_to_yyb.cxx',
                        'src/write_output/zzb_to_xt.cxx',
                        'src/write_output/lrho_to_xt_radial.cxx',
                        'src/write_output/xt_to_ws_radial.cxx'],
                target='blocks_3d',
                cxxflags=default_flags,
                use=use_packages
                )
