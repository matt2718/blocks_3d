#!/usr/bin/env wolframscript

tableDir="test/realistic_test"

<<"test/mathematica/radial_recursion.m"
<<"test/mathematica/derivative_conversions.m"

(* Four-fermion blocks *)

spins={1/2,1/2,1/2,1/2};

q4pmstructs=Flatten[Table[{{1/2,q2,q3,q4}*2,s},
                          {q2,-1/2,1/2},
                          {q3,-1/2,1/2},
                          {q4,-1/2,1/2},
                          {s,{-1,1}}],
                    3];
Delta12=0.12`200;
Delta43=0.543`200;
twojchoices=2*Table[j,{j,0,50}];
keptPoleOrders={20,25,30};
order=80;
Lambda=25;


Do[
        writePartialTables[tableDir, 2*spins,Delta12, Delta43, 2*j12, 2*j43, q4pm,
                           twojchoices, Lambda, keptPoleOrders, order];
       ,{j12,0,1}
       ,{j43,0,1}
       ,{q4pm,q4pmstructs}
];


Do[
        write\[Lambda]lr2zzb[tableDir,  2*spins,Delta12, Delta43, 2*j12, 2*j43, q4pm,
                             twoj, Lambda, kPO, order];
       ,{j12,0,1}
       ,{j43,0,1}
       ,{q4pm,q4pmstructs}
       ,{twoj,twojchoices}
       ,{kPO,keptPoleOrders}
];


(* Four-T blocks *)

spins={2,2,2,2};

q4pmstructs2d={#,+1}&/@{{2,2,2,2},{1,1,1,1},{1,2,1,2},{1,1,2,2},{2,1,1,2}};
q4pmstructs1d={#,+1}&/@{{0,0,0,0},{0,1,0,1},{0,2,0,2},{0,1,1,2},{1,0,1,2},
                        {0,0,1,1},{1,0,0,1},{0,0,-1,1},{-1,0,0,1}};
q4pmstructs0d={#,+1}&/@{{0,0,2,2},{2,0,0,2},{0,1,-1,2},{-1,1,0,2},{0,-1,1,2},
                        {1,-1,0,2},{1,-1,-1,1},{-1,-1,1,1}};
Delta12=0;
Delta43=0;
twojchoices=2*Table[j,{j,0,50}];
keptPoleOrders={20,25,30};
order=80;
Lambda=25;


Do[
        writePartialTables[tableDir, 2*spins,Delta12, Delta43, 2*j12, 2*j43, q4pm,
                           twojchoices, Lambda, keptPoleOrders, order];
       ,{j12,0,4}
       ,{j43,0,4}
       ,{q4pm,q4pmstructs2d}
];

Do[
        (* Note different function call: we only need radial
           deriative, no lambda-derivatives *)
        writeLogRLambdaDerivTable[tableDir,  2*spins,, Delta12, Delta43, 2*j12, 2*j43, q4pm,
                                  twojchoices, Lambda, 0, keptPoleOrders, order];
       ,{j12,0,4}
       ,{j43,0,4}
       ,{q4pm,q4pmstructs1d}
];

Do[
        (* Note different function call: we only need the value of the
           conformal block at the crossing-symmetric point *)
        writeLogRLambdaDerivTable[tableDir,  2*spins,, Delta12, Delta43, 2*j12, 2*j43, q4pm,
                                  twojchoices, 0, 0, keptPoleOrders, order];
       ,{j12,0,4}
       ,{j43,0,4}
       ,{q4pm,q4pmstructs0d}
];



(* For this example there is a post-processing step that commutes with
stage 2, and if performed before stage 2, would reduce the size of the
input to stage 2. *)

(* For simplicity we don't do this *)


(* Only the "2d" structures need to be stage-2 processed *)
(* Actually, we do need some variant of stage-2 processing for 0d and
1d structures, which is way simpler, however, than 2d. It is not
implemented, but the implementaiton is very simple given the current
stage-2 implementation. *)

Do[
        write\[Lambda]lr2zzb[tableDir,  2*spins,Delta12, Delta43, 2*j12, 2*j43, q4pm,
                             twoj, Lambda, kPO, order];
       ,{j12,0,4}
       ,{j43,0,4}
       ,{q4pm,q4pmstructs2d}
       ,{twoj,twojchoices}
       ,{kPO,keptPoleOrders}
];


(* Four-spin-2 blocks (hard) *)

spins={2,2,2,2};
q4pmstructsHard=DeleteCases[Flatten[Table[{{q1,q2,q3,q4},s},{q1,0,2},
                                          {q2,If[q1!=0,-2,0],2},
                                          {q3,If[{q1,q2}!={0,0},-2,0],2},
                                          {q4,If[{q1,q2,q3}!={0,0,0},-2,0],2},
                                          {s,{-1,1}}],
                                    4],
                            {{0,0,0,0},-1}];
Delta12=0.12`200;
Delta43=0.543`200;
twojchoices=2*Table[j,{j,0,50}];
keptPoleOrders={20,25,30};
order=80;
Lambda=25;


Do[
        writePartialTables[tableDir, 2*spins,Delta12, Delta43, 2*j12, 2*j43, q4pm,
                           twojchoices, Lambda, keptPoleOrders, order];
       ,{j12,0,4}
       ,{j43,0,4}
       ,{q4pm,q4pmstructs}
];


Do[
        write\[Lambda]lr2zzb[tableDir,  2*spins,Delta12, Delta43, 2*j12, 2*j43, q4pm,
                             twoj, Lambda, kPO, order];
       ,{j12,0,4}
       ,{j43,0,4}
       ,{q4pm,q4pmstructs}
       ,{twoj,twojchoices}
       ,{kPO,keptPoleOrders}
];
