(* ::Package:: *)

Get[FileNameJoin[{DirectoryName[$InputFileName],"util.m"}]];

GetRelative["conventions.m"];
GetRelative["cg_coefficients.m"];

(*

tildeWignerCoefficient[j,m,mp,n] computes the coefficient 
of y^{2n+Abs[m-mp]} in the series expansion of \tilde d^j_{m,mp}(-\theta)
in y = i sin (\theta/2).

\tilde d^j_{m,mp}(-\theta) is defined as

I^(mp-m)WignerD[{j,m,mp},-theta] 

Valid input always has 
* twoj,Nn\[GreaterEqual]0
* EvenQ[twoj-twomout]\[Equal]EvenQ[twoj-twompout]\[Equal]True 
* Abs[twomout],Abs[twompout]\[LessEqual]twoj

*)

(* (Int,Int,Int,Int) \[Rule] Real *)
tildeWignerCoefficient[twoj_,twomout_,twompout_,Nn_]:=Module[
	{twom0,kmax,n,twom,twomp},

	If[twomout + twompout >= 0,
	   twom = twomout; twomp = twompout;
         , twom = -twomout; twomp = -twompout;];
	twom0 = Max[twom, twomp];
	If[EvenQ[(twom + twomp)/2], kmax=(twom + twomp)/4,
           kmax=\[Infinity] ];
	
	Sqrt[(((twoj - twomp)/2)! * ((twoj - twom)/2)!)
             / (((twoj + twomp)/2)!((twoj + twom)/2)!)]
        * Sum[((twoj + twom0 + 2n)/2)!
              / (((twoj - twom0 - 2*n)/2)! * ((Abs[twomp-twom]+2n)/2)!n!)
              * Binomial[(twom + twomp)/4, Nn-n],
              {n, Max[0, Nn-kmax], (twoj - twom0)/2}]]

(*

sinhPowerDerivative[n,k] computes the value of
D[Sinh[\[Lambda]/2]^n,{\[Lambda],2k+n}]/.\[Lambda]\[Rule]0

Valid input has n,k\[GreaterEqual]0

*)
(* (Int,Int) \[Rule] Real *)
ClearAll[sinhPowerDerivative];
sinhPowerDerivative[0,k_]:=KroneckerDelta[k,0];
sinhPowerDerivative[n_,k_]/;n>0:=
        sinhPowerDerivative[n,k]=
        Sum[1/2^(2m+1) * sinhPowerDerivative[n-1,k-m] * Binomial[2k+n,2m+1],
            {m, 0, k}];


(*

tildeWignerCoefficient[2j,2m,2mp,n] computes the n-th derivative of \tilde d^j_{m,mp}(-\theta)
in \lambda=i \theta at \lambda=0.

\tilde d^j_{m,mp}(-\theta) is defined as

I^(mp-m)WignerD[{j,m,mp},-theta]


Valid input always has 
* twoj,Nn\[GreaterEqual]0
* EvenQ[twoj-twomout]\[Equal]EvenQ[twoj-twompout]\[Equal]True 
* Abs[twomout],Abs[twompout]\[LessEqual]twoj


(Int,Int,Int,Int)\[Rule]Real 
*)
tildeWignerDerivative[twoj_,twom_,twomp_,Nn_]:=Module[
        {sinhtable,n},

        If[Nn < Abs[twom - twomp]/2, Return[0];];
        If[OddQ[Nn - Abs[twom - twomp]/2], Return[0];];

        Sum[sinhPowerDerivative[2n + Abs[twom - twomp]/2,
                                (Nn - 2n - Abs[twom - twomp]/2)/2]
            * tildeWignerCoefficient[twoj, twom, twomp, n],
            {n, 0, (Nn - Abs[twom - twomp]/2)/2}
        ]];


(*
This computes the conversion matrix W between q- and SO(3)-basis three-pt functions.

This function appears to be the major bottleneck in computation of hInfinity, 
likely due to slow CG coefficient implementation.

UPDATE: I improved CG implementation a bit -- mathematica's implementation is suprisingly slow: it is beaten by the standard sum formula implemented in mathematica!
As a plus, now we don't use Mathematica's CG coefficients.
Currently contains a timing code that accumulates the W computation time in variable totalWtime.

TODO: It might be factor this into two matrices, one for left structs and one for right.
I am not sure why it was implemented with two together in the first place, but didn't fix it yet.

Valid input:

* leftSO3struct and leftQ3struct are valid for {twojs[[1]],twojs[[2]],twoj} [see struct_properties.m for definition of "valid"]
* rightSO3struct and rightQ3struct are valid for {twojs[[4]],twojs[[3]],twoj} [see struct_properties.m for definition of "valid"]
* twojs, twoj\[GreaterEqual]0
* EvenQ[twoj-twojs[[1]]-twojs[[2]]] && EvenQ[twoj-twojs[[4]]-twojs[[3]]]

*)
(* [{Int,Int,Int,Int},Int][SO3struct,SO3struct][Q3struct,Q3struct] \[Rule] Real *)
ClearAll[fWConversionMatrix,totalWtime];
totalWtime=0;
fWConversionMatrix[twojs_, twoj_][leftSO3struct_, rightSO3struct_][leftQ3struct_, rightQ3struct_]:=
        fWConversionMatrix[twojs, twoj][leftSO3struct, rightSO3struct][leftQ3struct, rightQ3struct]=
	Module[
                {twoj12 = leftSO3struct[[1]],
		 twoj120 = leftSO3struct[[2]],
		 twoj43 = rightSO3struct[[1]],
		 twoj430 = rightSO3struct[[2]],
		 twoqs = {leftQ3struct[[1]], leftQ3struct[[2]],
                          rightQ3struct[[2]], rightQ3struct[[1]]},
		 twoq = leftQ3struct[[3]],
		 twoqp = rightQ3struct[[3]], ii, startTime, result},
			
		startTime = AbsoluteTime[];

		(* Exponent is always integer for valid input *)
		result = (-1)^((twojs[[1]] + twojs[[4]] - 2*twoj + twoqs[[2]] + twoqs[[3]])/2)
                         * CGFormula[{twojs[[1]]/2,twoqs[[1]]/2},{twojs[[2]]/2,twoqs[[2]]/2},{twoj12/2,-twoq/2}]
(**CGFormula[{twoj12/2,-twoq/2},{twoj/2,twoq/2},{twoj120/2,0}]*)
                         * SpecialCG[twoj12/2,twoj120/2,twoj/2,twoq/2]
                         * CGFormula[{twojs[[4]]/2,twoqs[[4]]/2},{twojs[[3]]/2,twoqs[[3]]/2},{twoj43/2,-twoqp/2}]
(**CGFormula[{twoj43/2,-twoqp/2},{twoj/2,twoqp/2},{twoj430/2,0}]*)
                         * SpecialCG[twoj43/2,twoj430/2,twoj/2,twoqp/2]
                         * Sqrt[Binomial[twoj,(twoj+twoq)/2]
                                * Binomial[twoj,(twoj+twoqp)/2]
                                * Product[Binomial[twojs[[ii]],
                                                   (twojs[[ii]] + twoqs[[ii]])/2],
                                          {ii, 1, 4}]];
		
		totalWtime += AbsoluteTime[] - startTime;
		result
	];


(* 

Valid input always has 
* twoj\[GreaterEqual]0
* EvenQ[twoj-twom1]\[Equal]EvenQ[twoj-twom2]\[Equal]True 
* Abs[twom1],Abs[twom2]\[LessEqual]twoj

(Int,Int,Int) \[Rule] Real *)
ClearAll[wjmMatrix];
wjmMatrix[{twoj_, twom1_, twom2_}]:=
        wjmMatrix[{twoj, twom1, twom2}]=
        Module[{twoq = twom1, p = (twom2 + twoj)/2},
               2^(-twoj/2) (*Sqrt[Binomial[twoj, (twoj + twoq)/2]/Binomial[twoj, p]] *)
               * Sum[(-1)^l * Binomial[(twoj + twoq)/2, p - l]
                     * Binomial[(twoj - twoq)/2, l],
                     {l, 0, p}]];

(* 
This function computes the lambda-derivative of order n of 
f^{(p1,p2,p),(p4,p3,pp)}_{j,[q1,q2,q2,q4]}(theta)
where lambda=i theta.

In this function, we fix the normalization of the conformal block. 
It is the same as described in the paper 1907.11247 except for the phase.

The conformal block as described in 1907.11247 is the conformal block here times phaseFactor,
where phaseFactor is 1 if j is integer and i if j is half-integer, i.e. 
phaseFactor=i^{F_0}. 

Valid input has:

* n\[GreaterEqual]0
* EvenQ[Total[twojs]]
* EvenQ[twoj-twojs[[1]]-twojs[[2]]] && EvenQ[twoj-twojs[[4]]-twojs[[3]]]
* leftQ3struct is valid for {js[[1]],js[[2]],j}
* rightQ3struct is valid for {js[[4]],js[[3]],j}
* q4struct is valid for twojs

[{Int,Int,Int,Int},Int][Q3struct,Q3struct,Q4struct][Int] \[Rule] Real

*)
ClearAll[fQBasisDerivative];
fQBasisDerivative[twojs_, twoj_][leftQ3struct_, rightQ3struct_, q4struct_][n_]:=
        fQBasisDerivative[twojs, twoj][leftQ3struct, rightQ3struct, q4struct][n]=
        Module[{twops, F43, twop, twopp}, 

               twops = {leftQ3struct[[1]], leftQ3struct[[2]],
                        rightQ3struct[[2]], rightQ3struct[[1]]};
               twop = leftQ3struct[[3]];
               twopp = rightQ3struct[[3]];
               F43 = Mod[twojs[[4]], 2] + Mod[twojs[[3]], 2]; 
(* We use F43 instead of F430 to have a real block. See below. *)

(* The expression F430-twojs[[3]]-twojs[[4]]-F0=F43-twojs[[3]]-twojs[[4]] is always even.*)
               twoPointB[twoj]^-1 (-1)^((F43 - twojs[[3]] - twojs[[4]])/2)
               * wjmMatrix[{twojs[[1]], twops[[1]], q4struct[[1]]}]
               * wjmMatrix[{twojs[[2]], twops[[2]], q4struct[[2]]}]
               * wjmMatrix[{twojs[[3]], -twops[[3]], q4struct[[3]]}]
               * wjmMatrix[{twojs[[4]], -twops[[4]], q4struct[[4]]}]
               * Binomial[twoj, (twoj-twop)/2]^(-1/2)
               * Binomial[twoj, (twoj-twopp)/2]^(-1/2)
               * tildeWignerDerivative[twoj, -twopp, -twop, n]];


(*
This function computes the lambda-derivative of order n of 
f^{(j12,j120),(j43,j430)}_{j,[q1,q2,q2,q4]}(theta)
where lambda=i theta.

[{Int,Int,Int,Int},Int][SO3struct,SO3struct,Q4struct][Int] \[Rule] Real

* n\[GreaterEqual]0
* EvenQ[Total[twojs]]
* EvenQ[twoj-twojs[[1]]-twojs[[2]]] && EvenQ[twoj-twojs[[4]]-twojs[[3]]]
* leftSO3struct is valid for {js[[1]],js[[2]],j}
* rightSO3struct is valid for {js[[4]],js[[3]],j}
* q4struct is valid for twojs

*)

ClearAll[fSO3BasisDerivative];
fSO3BasisDerivative[twojs_, twoj_][leftSO3struct_, rightSO3struct_, q4struct_][n_]:=
        fSO3BasisDerivative[twojs, twoj][leftSO3struct, rightSO3struct, q4struct][n]=
        Module[{leftQ3structSet, rightQ3structSet, leftQ3struct, rightQ3struct},

               leftQ3structSet = Flatten[Table[{twop1, twop2, -twop1 - twop2},
                                               {twop1, -twojs[[1]], twojs[[1]], 2},
                                               {twop2, -twojs[[2]], twojs[[2]], 2}], 1];
               rightQ3structSet=Flatten[Table[{twop4, twop3, -twop4-twop3},
                                              {twop4, -twojs[[4]], twojs[[4]], 2},
                                              {twop3, -twojs[[3]], twojs[[3]], 2}], 1];
               leftQ3structSet=DeleteCases[leftQ3structSet,
                                           {_,_,twop_}/;Abs[twop]>twoj];
               rightQ3structSet=DeleteCases[rightQ3structSet,
                                            {_,_,twop_}/;Abs[twop]>twoj];

               Sum[fWConversionMatrix[twojs, twoj][leftSO3struct, rightSO3struct][leftQ3struct, rightQ3struct]
                   * fQBasisDerivative[twojs, twoj][leftQ3struct, rightQ3struct, q4struct][n],
                   {leftQ3struct, leftQ3structSet},
                   {rightQ3struct, rightQ3structSet}]]
