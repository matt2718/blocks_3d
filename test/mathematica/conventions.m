(* ::Package:: *)

(* This gives the a0 constant in the normalization cO=(4/a0)^Dealta * b_j *)

twoPointA0=1;


(* This gives the b_j constant in the normalization cO=(4/a0)^Dealta * b_j *)

(* Int\[Rule]Real *)
twoPointB[twoj_]:=1;


(* This combines the two normalization constants into cO *)
(* (Real,Int) \[Rule] Real *)
cNorm[\[CapitalDelta]_,twoj_]:=(4/twoPointA0)^\[CapitalDelta]*twoPointB[twoj/2];


rCrossing=N[3-2Sqrt[2],prec];


(* j can be either an integer or a half-integer. Thus, we mostly use the
   variable twoj=2*j, which is an integer. These functions are for convenience
   when working with twoj. *)
   
jCeil[twoj_]:=Quotient[twoj+1,2];
jFloor[twoj_]:=Quotient[twoj,2];
bosonic[twoj_]:=Mod[twoj,2]==0;
fermionic[twoj_]:=Mod[twoj,2]==1;
