(* ::Package:: *)

Get[FileNameJoin[{DirectoryName[$InputFileName],"util.m"}]];

GetRelative["conventions.m"];


GetRelative["wigner_derivatives.m"];


GetRelative["structure_properties.m"];


(* 
THIS FUNCTION IS NOT USED IN THE COMPUTATION. 
It only provides a simpler (but slower) reference implementation of hInfinityPrefactorDerivatives.
This comment is also relevant for the definition of hInfinityPrefactorDerivatives

This function computes the following data. Let g be

g[r,lambda] = 
(1-r*r)^(-1/2)Exp[2lambda*c](1+r*Exp[lambda])^a(1-r*Exp[lambda])^b(1+r*Exp[-lambda])^ab(1-r*Exp[-lambda])^bb

then define a[n,k] by

D[g[r,lambda],{lambda,k}]/.lambda\[Rule]0=Sum[a[n,k]*r^n,{n,0,Infinity}]

Then hInfinityPrefactor[order,{a,ab,b,bb,c},k] is equivalent to 

hInfinityPrefactor[order,{a,ab,b,bb,c},k] \[Equal] Table[a[n,k],{n,0,order}]

*)

(*
(Int,{Real,Real,Real,Real,Real},Int) \[Rule] Real[order+1]
*)

hInfinityPrefactor[order_,{a_,ab_,b_,bb_,c_},k_]:=Module[
        {m1,m2,m3,n},
        powerSeriesTimes[Table[Sum[((m1 - m2 + m3 - (n - m1 - m2 - m3) + 2*c))^k
                                   * (-1)^(n - m1 - m2)
                                   * Binomial[a, m1]
                                   * Binomial[ab, m2]
                                   * Binomial[b, m3]
                                   * Binomial[bb, n - m1 - m2 - m3],
                                   {m1, 0, n},
                                   {m2, 0, n - m1},
                                   {m3, 0, n - m1 - m2}],
                               {n, 0, order}],
                         oneMinusRSquared[order]]]

(*
This function computes the following data. 
Let g[r,lambda] be defined by

g[r,lambda] = 
(1+r*Exp[lambda])^a(1-r*Exp[lambda])^b(1+r*Exp[-lambda])^ab(1-r*Exp[-lambda])^bb

which is a bit different from g in comments above hInfinityPrefactor.
Then we can write

g[r,lambda]=Sum[a[n,j]r^n Exp[(2j-n)lambda],{n,0,Infinity},{j,0,n}].

This function returns

Table[a[n,j],{n,0,order},{j,0,n}].

(Int,{Real,Real,Real,Real}) \[Rule] List of Lists of Reals (see above for dimensions).

*)

ClearAll[hInfinityDoubleSeriesTable]
hInfinityDoubleSeriesTable[order_, {a_, ab_, b_, bb_}]:=
        hInfinityDoubleSeriesTable[order,{a,ab,b,bb}]=
        Module[{m2,m3,j,n},
	       Table[Table[Sum[(-1)^(n - j - m3 - m2)
                               * Binomial[a, j - m3]
                               * Binomial[ab, m2]
                               * Binomial[b, m3]
                               * Binomial[bb, n - j - m2],
			       {m2, 0, n - j},{m3, 0, j}],
		           {j, 0, n}],
	             {n, 0, order}]];


(*

This function is an optimized version of hInfinityPrefactor, and is equivalent to

hInfinityPrefactorDerivatives[order,{a,ab,b,bb,c},kmax] \[Equal]
	Table[hInfinityPrefactor[order,{a,ab,b,bb,c},k],{k,0,kmax}];
	

(Int,{Real,Real,Real,Real,Real},Int) \[Rule] Real[kmax+1,order+1]


*)

ClearAll[hInfinityPrefactorDerivatives];
hInfinityPrefactorDerivatives[order_,{a_,ab_,b_,bb_,c_},kmax_]:=
        hInfinityPrefactorDerivatives[order,{a,ab,b,bb,c},kmax]=
        Module[{j,n,k},
               Table[powerSeriesTimes
                     [Table
                      [hInfinityDoubleSeriesTable
                       [order, {a, ab, b, bb}][[n + 1]].Table[If[2*j - n + 2*c == 0 && k == 0,
                                                                 1, (2*j - n + 2*c)^k],
                                                              {j, 0, n}],
                       {n,0,order}],
                      oneMinusRSquared[order]],
                     {k, 0, kmax}]];

(*

This function multiplies two power series.

(Real[n1], Real[n2]) \[Rule] Real[Min[n1,n2]]

*)

powerSeriesTimes[a_,b_]:=
	Module[{len = Min[Length[a], Length[b]]},
	       Table[Sum[a[[m + 1]]b[[n - m + 1]],
                         {m, 0, n}],{n, 0, len - 1}]];

(*

This function computes the power series of (1-r*r)^(-1/2) in r. 
Note that half of the coefficients are zero, and so we are not storing it in the most efficient way.

(Int order) \[Rule] Real[order+1]

*)

oneMinusRSquared[order_]:=
        Table[If[EvenQ[n], Pochhammer[1/2, n/2]/Pochhammer[1,n/2], 0],
              {n, 0, order}]


(*

This is the final function which computes r-series of lambda-derivatives of hInfinity.

hInfinity_here = i^{-F0} hInifnity_paper, where F0 is the fermion number of exchanged op; see wigner_derivatives.m

hInfinity_here is real.

Valid input:
* towjs,twoj,order,n\[GreaterEqual]0
* twoj has the same parity as twojs[[1]]+twojs[[2]] and twojs[[3]]+twojs[[4]]
* Total[twojs] is even
* leftSO3struct is valid for {twojs[[1]],twojs[[2]],twoj} [see struct_properties.m for definition of "valid"]
* rightSO3struct is valid for {twojs[[4]],twojs[[3]],twoj} [see struct_properties.m for definition of "valid"]
* twoqs is valid for twojs [see struct_properties.m for definition of "valid"]

[{Int,Int,Int,Int},Int,Real,Real][SO3struct,SO3struct,Q4pt][Int order,Int]\[Rule]Real[order+1]

SO3struct = {Int,Int}
Q4struct = {Int,Int,Int,Int}

*)

ClearAll[hInfinityFullDerivative];
hInfinityFullDerivative[twojs_, twoj_, Delta12LP_, Delta43LP_][leftSO3struct_, rightSO3struct_, q4struct_][order_, n_]:=
        hInfinityFullDerivative[twojs, twoj, Delta12LP, Delta43LP][leftSO3struct, rightSO3struct, q4struct][order, n]=
        Module[
                {
                        Delta12 = highPrecision[Delta12LP],
                        Delta43 = highPrecision[Delta43LP],
                        alpha, beta, alphab, betab,
                        a, b, ab, bb, c,
                        fderivatives, k,
                        prefactorderivatives
                },

                alpha = (-Delta12 - q4struct[[2]]/2 + q4struct[[1]]/2)/2;
                alphab = (-Delta12 + q4struct[[2]]/2 - q4struct[[1]]/2)/2;
                beta = (-Delta43 - q4struct[[3]]/2 + q4struct[[4]]/2)/2;
                betab = (-Delta43 + q4struct[[3]]/2 - q4struct[[4]]/2)/2;

                a = alpha + beta - 1/2 - q4struct[[1]]/2- q4struct[[2]]/2;
                b = -alpha - beta - 1/2;
                ab = alphab + betab - 1/2 + q4struct[[1]]/2 + q4struct[[2]]/2;
                bb = -alphab - betab - 1/2;
                c = q4struct[[1]]/4 + q4struct[[2]]/4;

                fderivatives = Table
                               [fSO3BasisDerivative
                                [twojs,twoj][leftSO3struct,rightSO3struct,q4struct][k],
                                     {k, 0, n}];

                prefactorderivatives = hInfinityPrefactorDerivatives[order,{a, ab, b, bb, c}, n];

                fderivatives = N[fderivatives//Simplify,prec]; (* Mathematica fails on expressions such as N[(47 Sqrt[5/154])/12-(5 Sqrt[10/77])/3-Sqrt[35/22]/12,200] (which are equal to 0...) *)
                prefactorderivatives = N[prefactorderivatives//Simplify,prec];

                Sum[Binomial[n, k] * fderivatives[[k + 1]]
                    * prefactorderivatives[[n - k + 1]], {k, 0, n}]]


(*

This function produces an array of hInfinityFullDerivative's, which will then be used by the reucursion algorithm.

Valid input has
* twojs,twoj12,twoj43,twojMax,order,n\[GreaterEqual]0
* q4struct is valid for twojs [see struct_properties.m for definition of "valid"]
* twoj12 is in so3Tensor[twojs[[1]],twojs[[2]]]
* twoj43 is in so3Tensor[twojs[[4]],twojs[[3]]]
* twojMax-twojs[[1]]-twojs[[2]] is even
* Total[twojs] is even

[{Int,Int,Int,Int},Real,Real][Int,Int,Q4struct][Int,Int,Int] \[Rule] Array of Reals, see the returned Table for structure

*)

hInfinityVector[twojs_,Delta12LP_,Delta43LP_][twoj12_,twoj43_,q4struct_][twojMax_,order_,n_]:=
        Module[{paritytypes,pseudoparitytypes,twojValues,
                twoj,ppindex,rOrder,j120,j430},

(* Based on the 4pt structure we figure out whether we compute {even,even} and {odd,odd} pairs
of 3pt structures or {even,odd} and {odd,even}. This is encoded by paritytypes.
We then translate these parities to pseudoparities. *)
               paritytypes = parityTypes[[fourPointParity[twojs, q4struct] + 1]];
               pseudoparitytypes = 
               {pseudoParityToParity[twojs[[1]], twojs[[2]], twoj12]/@#[[1]],
                pseudoParityToParity[twojs[[4]], twojs[[3]], twoj43]/@#[[2]]}&@
                                                                             Transpose[paritytypes];
               pseudoparitytypes = Transpose[pseudoparitytypes];

               twojValues[rOrder_]:=Range[Mod[twoj12, 2],
                                          twojMax + 2*(order - rOrder), 2];

(* Print["twojValues: ",twojValues[1]]; *)

               Table
               [hInfinityFullDerivative
                [twojs, twoj, Delta12LP, Delta43LP][{twoj12, twoj120},
                                                    {twoj43, twoj430},
                                                    q4struct][order,n][[rOrder+1]],
                {ppindex, {1, 2}} (* May make more sense to move this index two levels down, and same in radial_recursion.m *)
               ,{rOrder, 0, order}
               ,{twoj, twojValues[rOrder]}
               ,{twoj120, so3TensorParity[pseudoparitytypes[[ppindex,1]],
                                          twoj12, twoj]}
               ,{twoj430, so3TensorParity[pseudoparitytypes[[ppindex,2]],
                                          twoj43, twoj]}
               ]
        ]
