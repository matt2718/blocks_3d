(* ::Package:: *)

Get[FileNameJoin[{DirectoryName[$InputFileName],"util.m"}]];


GetRelative["conventions.m"];


(* This notebook encodes the table in the "classification of poles" section
   4.1 of the summary. *)
   
(* Equivalently, Table 1 of 1907.11247 *)

poleFamilies={"I","II","III","IV"};

(* poleParity : Family \[Rule] Int *)
(* PK: changed to 0 being even and 1 being odd *)
poleParity["I"]:=0;
poleParity["II"]:=0;
poleParity["III"]:=0;
poleParity["IV"]:=1;

(* poleDelta : (Int,(Family,Int)) \[Rule] Real *)
poleDelta[twoj_,{"I",k_}]:=1-twoj/2-k;
poleDelta[twoj_,{"II",k_}]:=2+twoj/2-k;
poleDelta[twoj_,{"III",k_}]:=3/2-k;
poleDelta[twoj_,{"IV",k_}]:=2-k;

(* poleTwoJ : (Int,(Family,Int)) \[Rule] Int *)
poleTwoJ[twoj_,{"I",k_}]:=twoj+2k;
poleTwoJ[twoj_,{"II",k_}]:=twoj-2k;
poleTwoJ[twoj_,{"III",k_}]:=twoj;
poleTwoJ[twoj_,{"IV",k_}]:=twoj;

(* poleShift : (Family,Int) \[Rule] Int *)
poleShift[{"I",k_}]:=k;
poleShift[{"II",k_}]:=k;
poleShift[{"III",k_}]:=2k;
poleShift[{"IV",k_}]:=2k-1

(* poleRangeK : (Family,Int,Int,Int,Int) \[Rule] Int *)
(* PK: changed so that maxOrder is always taken into account *)
poleRangeK[type_,twoj_,twoj12_,twoj43_,maxOrder_]:=Switch[type,
  "I", maxOrder,
  "II", Min[jFloor[twoj],maxOrder],
  "III", If[bosonic[twoj],
    Quotient[maxOrder,2],
    Min[jFloor[twoj],jFloor[twoj12],jFloor[twoj43],Quotient[maxOrder,2]]
    ],
  "IV", If[bosonic[twoj],
    Min[jFloor[twoj],jFloor[twoj12],jFloor[twoj43],Quotient[maxOrder+1,2]],
    Quotient[maxOrder+1,2]
    ]
  ];
