#!/bin/bash

numthreads=4
twojs="0-50"

# Four-fermion
spins="0.5, 0.5, 0.5, 0.5"
delta12="0.12"
delta43="0.543"
q1=0.5
for q2 in -0.5 0.5
do
    for q3 in -0.5 0.5
    do
        for q4 in -0.5 0.5
        do
            for s in -1 1
            do
                for j12 in 0 1
                do
                    for j43 in 0 1
                    do
                        echo "Four fermion" $q1 $q2 $q3 $q4 $s $j12 $j43
                        /usr/bin/time ./build/blocks_3d --j-external "$spins" --j-internal "$twojs" --j-12 $j12 --j-43 $j43 --delta-12 "$delta12" --delta-43 "$delta43" --four-pt-struct "${q1}, ${q2}, ${q3}, ${q4}" --four-pt-sign "${s}" --order 80 --lambda 25 --kept-pole-order 30 --num-threads $numthreads --precision 665 -o test/benchmark/zzb_${q1}_${q2}_${q3}_${q4}_${s}_${j12}_${j43}_{}.json
                    done
                done
            done
        done
    done
done
